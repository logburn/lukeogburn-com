<!doctype html>
<html lang=en>
<?php include "head.php"; ?>
<body><div id=body>

<?php include "top.php"; ?>

  <div id=content>
    <p>My name is <span class="letter">L</span><span class="letter">u</span><span class="letter">k</span><span class="letter">e</span> <span class="letter">O</span><span class="letter">g</span><span class="letter">b</span><span class="letter">u</span><span class="letter">r</span><span class="letter">n</span>, and I'm a software developer studying Computer Science at James Madison University. I'm interested in technologies that give power to the common people, such as encryption and federation. I'm always on the lookout for new ideas or projects that fit this category. On that note, I am a strong supporter of open source. I also practice what I preach: almost all of my code is available on my <a target=_blank href=https://github.com/logburn>GitHub</a> and is licensed under the <a target=_blank href=https://www.gnu.org/licenses/gpl-3.0.en.html>GNU GPLv3</a>. On the flip side, I also despise those who seek to control others, such as the Chinese Communist Party, or even the United State's own far-overreaching practices.
    </p>
</div>

  <h2 class=name>CONNECT</h2>
  <div class=network>
      <a class=networkLink target=_blank href=mailto:lukeogburn@gmail.com>GMAIL</a>
      <a class=networkLink target=_blank href=mailto:lukeogburn@protonmail.com>PROTON</a>
      <a class=networkLink target=_blank href=https://github.com/logburn>GITHUB</a>
      <a class=networkLink target=_blank href=https://gitlab.com/logburn>GITLAB</a>
      <a class=networkLink target=_blank href=https://stackoverflow.com/users/13157956/luke-ogburn>STACKOVERFLOW</a>
      <a class=networkLink target=_blank href=https://linkedin.com/in/logburn>LINKEDIN</a>
      <a class=networkLink target=_blank href=https://pixelfed.social/logburn>PIXELFED</a>
      <a class=networkLink target=_blank href=resume/>RESUME</a>
  </div>
  <div class=bottom>
    <a href=pubkey.txt target=_blank>PGP key</a>
  </div>
  <a rel="me" href="https://mastodon.online/@logburn" style=display:none>Mastodon</a>
</div></body>
</html>
